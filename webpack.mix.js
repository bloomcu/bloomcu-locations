// webpack.mix.js

let mix = require('laravel-mix');
require('laravel-mix-polyfill');


mix.options({
    postCss: [require('autoprefixer')]
});
// mix.js('src/app.js', 'dist').setPublicPath('dist');
// mix.css('assets/src/frontend/styles/index.scss', 'dist');
mix
    .sass('assets/src/frontend/styles/index.scss', 'dist/frontend/css/frontend.css', {
        sassOptions: {
            outputStyle: 'compressed'
        }
    })
    .sass('assets/src/admin/styles/index.scss', 'dist/admin/css/admin.css', {
        sassOptions: {
            outputStyle: 'compressed'
        }
    })
    .js('assets/src/frontend/js/location-finder.js', 'dist/frontend/js/location-finder.js',{

    })
    .polyfill({
        enabled: true,
        targets: '> 0.5%, last 2 versions, not dead' 
     });;