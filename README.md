# BloomCU Locations

BloomCU Locations Plugin. A WordPress Plugin by BloomCU.

Build with [Laravel Mix](https://laravel-mix.com/docs/6.0/installation)
Minimum node version 18 - tested with v18.16.0

nvm use 18
npm install
npm run dev



## Changelog
**1.5.49**
*Release Date - Jan 25, 2025 --Bryan*
*Branch Download URL for v2 sites (codybloom) - This checks the Base Theme version, and sets PUC to download from the correct build path if greater than version 2

**1.5.49**
*Release Date - Jan 22, 2025 --Bryan*
*fix spacing when info modal is active, remove css icon and replace with inline SVG

**1.5.18**
*Release Date - Oct 11, 2022 --Bryan*
*improve frontend localization-quick test of rebuild*

**1.5.17**
*Release Date - July 5,2022 --Bryan*
*Update for Walgreens v2 API*

**1.5.1**
*Release Date - August 13, 2020 -- Bryan*

*Fix for missing drive-thru hours label field

**1.5.0**
*Release Date - August 10, 2020 -- Rashaan*

*Added custom fields to control coop branches/atms active states.

**1.4.14**
*Release Date - July 31, 2020 -- Bryan*

*move info modal to the page footer

**1.4.13**
*Release Date - June 17, 2020 -- Bryan*

*Add option for map style selection

**1.4.12**
*Release Date – May 1, 2020 - Bryan*

*make sort order selectable in options

**1.4.11**
*Release Date – May 1, 2020 - Bryan*

*Reverse sort by type

**1.4.10**
*Release Date – May 1, 2020 - Bryan*

*Use default sort order (branch type -> distance )

**1.4.9**
*Release Date – April 16, 2020 - Rashaan*

* Improve ADA compliance attributes

**1.4.8**
*Release Date – March 18, 2020 - Bryan*

* Use current zoom when nav search options are toggled

**1.4.7**
*Release Date – February 26, 2020 - Bryan*

* Add info modal

**1.4.5**
*Release Date – January 31, 2020 - Bryan*

* Update stylesheet versioning

**1.4.4**
*Release Date – January 22, 2020 - Rashaan*

* Fixed ADA issues for image and header tags

**1.4.3**
*Release Date – December 2, 2019 - Rashaan*

* Fix map loading issue from missing file in version 1.4.2 export

**1.4.2**
*Release Date – November 18, 2019 - Rashaan*

* Wordpress Plugin Updater added

**1.4.1**
*Release Date – October 29, 2019 - Harmon*

* Fix missing branch and ATM filter title fields
* Add location subtitle

**1.4.0**
*Release Date – October 24, 2019 - Harmon*

* Add Walgreens API
* Add search from query string

**1.3.1**
*Release Date – October 21, 2019 - Rashaan*

* Update labels to translatable strings

**1.3.0**
*Release Date – October 14, 2019 - Harmon*

* Add ability to customize branch and atm filters

**1.2.1**
*Release Date – August 20, 2019 - Harmon*

* Fix near me reseting map position

**1.2.0**
*Release Date – August 16, 2019 - Harmon*

* Add setting for API search radiuses
* Add setting for starting map zoom
* Add setting for locator title
* Hide all filters if only one filter is enabled
* Re-organize Location Settings tabs

**1.1.2**
*Release Date – July 31, 2019 - Bryan*

* Sort by distance and zip latest
* Update z-index on map pins
* Minor fixes on phone numbers and hours

**1.1.1**
*Release Date – July 31, 2019 - Bryan*

* Add phone numbers repeater
* Move frontend scripts to footer

**1.1.0**
*Release Date – July 7, 2019 - Harmon*

* Added drive-thru hours to plugin locations
* Added settings options
* Plugin locations filters can be enabled/disabled
* CO-OP branch and ATM filters can be enabled/disabled
* Made near me a button rather than a filter
* Fixed location phone button hover state

**1.0.0**
*Release Date – June 25, 2019 - Harmon*

* Added COOP Shared branches
* Formatted main phone number as button

**0.0.1**
*Release Date – May 28, 2019 - Harmon*

* Initial push of plugin in development

# Developers
Need to extend this plugin or fix a bug, here's what you need to know.

## ACF

**Edit plugin ACF**

1. First import the latest two json files within the `/acf-json` directory of the plugin using the ACF plugin tools.
2. Disable the registration of those field groups in `/includes/class-post-type.php`
3. You can now use the ACF plugin to edit/extend the plugins ACF.

## REST API

**Add new fields to API output**
Add new fields to the plugins REST API within `/includes/class-rest-api.php`

## JS

Edit the location finder JS within `/assets/src/frontend/js/location-finder.js`

**Get all locations**
Locations are queried from the plugin's REST API via `loadGeoBranches()`

**Show single location**
Individual locations are rendered via `openBranchInfo()`

## Ready for Production

1. Export ACF groups as JSON and back them up in `/acf-json`
2. Export ACF groups as PHP and register them in `/includes/class-post-type.php`
3. Run `npm run build`
4. Run `npm run zip`
5. The production ready plugin is a .zip within plugin directory
