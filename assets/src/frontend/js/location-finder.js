//--------------------------------------------------------------
// Location Finder
//--------------------------------------------------------------

//
// Main Settings
//
var site_url = locations_data_layer.site_url;
// console.log(site_url);
// Filters
var enable_branches = locations_data_layer.enable_branches;
var enable_atms = locations_data_layer.enable_atms;
var enable_coop_branches = locations_data_layer.enable_coop_branches;
var enable_coop_atms = locations_data_layer.enable_coop_atms;
var enable_walgreens = locations_data_layer.enable_walgreens;
var map_styles = locations_data_layer.map_styles;


// Near me
var map_near_me_icon = locations_data_layer.map_near_me_icon;

// Branches
if (enable_branches) {
    var filter_our_branches_icon = locations_data_layer.filter_our_branches_icon;
    var map_our_branches_icon = locations_data_layer.map_our_branches_icon;
}

// ATMs
if (enable_atms) {
    var filter_our_atms_icon = locations_data_layer.filter_our_atms_icon;
    var map_our_atms_icon = locations_data_layer.map_our_atms_icon;
}

// COOP Branches
if (enable_coop_branches) {
    var filter_coop_branches_icon = locations_data_layer.filter_coop_branches_icon;
    var map_coop_branches_color = locations_data_layer.map_coop_branches_color;
}

// COOP ATMs
if (enable_coop_atms) {
    var filter_coop_atms_icon = locations_data_layer.filter_coop_atms_icon;
    var map_coop_atms_color = locations_data_layer.map_coop_atms_color;
}

// Walgreens
if (enable_walgreens) {
    var filter_walgreens_icon = locations_data_layer.filter_walgreens_icon;
    var map_walgreens_icon = locations_data_layer.map_walgreens_icon;
}

// Map
var starting_lat = locations_data_layer.starting_lat;
var starting_lng = locations_data_layer.starting_lng;
var starting_zoom = parseInt(locations_data_layer.starting_zoom);

// Search Radius
var radius_plugin = locations_data_layer.radius_plugin;
var radius_coop = locations_data_layer.radius_coop;

var googleApiKey = 'AIzaSyAwOXyC6lxFn0OkRzISyeiDDr0E_RyBJPQ';

var branches = '',
    coop_branches = '',
    myLocation = 0,
    lat = '',
    long = '',
    myLat = '',
    myLong = '',
    searching = 0;

let container = document.getElementById('bloom-widget');
//
// Init the Map
//
var geocoder,
    markers = [],
    infowindow;
    
  
// Entry point to script, called by script enqueue callback
function initMap() {
  let el = document.getElementById('map')
  
  geolocation_active = 0;
  geocoder = new google.maps.Geocoder();
  
  // TODO: Need to move google map script out of footer and enqueue it in the footer
  map = new google.maps.Map(el, {
    center: {
      lat: 0,
      lng: 0
    },
    styles: map_styles,
    zoom: starting_zoom,
    disableDefaultUI: true, // Disable all map controls
    zoomControl: true // Enable Zoom
  });
  infowindow = new google.maps.InfoWindow();

  // Get default location branches
  $('.branch-list-container').html('');

  // My Location Marker
  myLocation = 1;
  centerMapSearchLocation(starting_lat, starting_lng);

  //
  // Load GeoBranches, and get back an array of promises
  //
  let requests = loadGeoBranches(starting_lat, starting_lng, starting_zoom);
  addLoader(container, requests);
  Promise.allSettled(requests).then(function(values){
    for(let i = 0; i < values.length; i++) {
      if(values[i].status === "fulfilled") {
        values[i].value.cb(values[i].value.data);
      }
      
    }
    getBranchesByType();
    
  });
  // $.when.apply($,requests).done(function(){
  //   // once promises are fulfilled, filter them
  
  // })
}

function addLoader(container, my_promise) {
  container.classList.remove('loaded');
  container.classList.add('loading');
  let loader = container.getElementsByClassName('bloom-widget-loader');
  let loader_track = loader[0].getElementsByClassName('bloom-widget-loader__track');
  let done = 0;
  loader_track[0].style.width = 0+'%';
  let loader_timer = setInterval(function(){
    if (done < 90 ) {
      done += 10;
      loader_track[0].style.width = done+'%';
    } else {
      clearInterval(loader_timer);
    }
  }, 400);

  Promise.allSettled(my_promise).then(function(){
    clearInterval(loader_timer);
    loader_track[0].style.width = '100%';
    container.classList.remove('loading');
    container.classList.add('loaded');
  })
  
}


let coopBranchPromise = function(lat, long, zoom){
    return new Promise((resolve, reject) => {
    $.ajax({
      type: "GET",
      url: site_url + '/wp-json/locations/v1/coop?' + 'loctype=S' + '&latitude=' + lat + '&longitude=' + long + '&maxRadius=' + radius_coop,
    }).done(function(data){
      resolve({
        data: data,
        cb: function(data) {
          var json = JSON.parse(data);
          coop_branches = json.response.locations;
          if (coop_branches.length) {
            coop_branches.forEach(function(branch) {
              setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
              listBranch(branch, branch.locatorType);
            });
          }
          // map.setCenter(myLocationMarker);
          map.setZoom(zoom);
        }
      })
    })
  })
};

let coopATMPromise = function(lat, long, zoom) {
  return new Promise((resolve, reject) => {
    $.ajax({
      type: "GET",
      url: site_url + '/wp-json/locations/v1/coop?' + 'loctype=A' + '&latitude=' + lat + '&longitude=' + long + '&maxRadius=' + radius_coop,
    }).done(function(data) {
      resolve({
        data: data,
        cb: function(data) {
          var json = JSON.parse(data);
          coop_atms = json.response.locations;
  
          coop_atms.forEach(function(branch) {
            setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
            listBranch(branch, branch.locatorType);
          });
          // map.setCenter(myLocationMarker);
          map.setZoom(zoom);
        }
      })
    })
  });
}

let branchLocationPromise = function(lat, long, zoom){
  return new Promise((resolve, reject) => {
    $.ajax({
      type: "GET",
      url: site_url + '/wp-json/locations/v1/nearby?' + 'latitudeFrom=' + lat + '&longitudeFrom=' + long + '&search_radius=' + radius_plugin,
    }).done(function(data) {
      resolve({
        data: data,
        cb: function(data) {
          branches = data;
          var lastMarker = '';
          var sort_by = locations_data_layer.sort_by;
          var sort_order = locations_data_layer.sort_order;
          if(sort_by == 'none') {
            branches.sort(function(a, b) {
              return a.distance - b.distance;
            });
          } else {
            branches.sort(function(a, b) {
              // console.log(location1[sort_by].localeCompare(location2[sort_by]))
              if (a[sort_by].toLowerCase() < b[sort_by].toLowerCase()) {
                if(sort_order == 'ASC') {
                  return -1;
                }
                return 1
              } else if (a[sort_by].toLowerCase() > b[sort_by].toLowerCase()) {
                if(sort_order == 'ASC') {
                  return 1;
                }
                return -1
              } else {
                return a.distance - b.distance;
              }
            });
          }
         

          branches.forEach(function(branch) {
            setMarker(branch.google_map.lat, branch.google_map.lng, branch.title, branch.type, branch);
            listBranch(branch, branch.type);
          });
          map.setZoom(zoom);
        }
      })
      
    });
  });
}

let wagLocationPromise  = function(lat, long, Zoom) {
  return new Promise((resolve, reject) => {
    $.ajax({
      type: "GET",
      url: site_url + '/wp-json/locations/v1/walgreens?' + 'latitudeFrom=' + lat + '&longitudeFrom=' + long + '&search_radius=' + radius_plugin,
    }).done(function(data) {
      resolve({
        data: data,
        cb: function(data) {
          walgreensLocations = data;
          walgreensLocations.sort(function(location1, location2) {
            return location1.distance - location2.distance
          });
          walgreensLocations.forEach(function(walgreensLocation) {
            setMarker(walgreensLocation.stlat, walgreensLocation.stlng, walgreensLocation.locationName, 'Walgreens', walgreensLocation);
            listBranch(walgreensLocation, 'Walgreens');
          });
          // map.setCenter(myLocationMarker);
          map.setZoom(Zoom);
        }
      })
    })
  });
}



//
// Check for "locate" url param
//
$( document ).ready(function() {

    var locateParam = getUrlParams()["locate"];

    if (locateParam != undefined) {
        locateParam = decodeURIComponent(locateParam.replace(/\+/g, ' '));

        if (locateParam.length > 0) {
    		$('.search-input').val(locateParam);
    		searchBranches();
    	}
    }

});

//
// Get URL param
//
function getUrlParams()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

//
// Search Function
//
function searchBranches(event) {

  if (event)
	event.preventDefault();

  // Set searching ON.
  searching = 1;

  // Hide search button.
  $('.search-button').hide();

  // Show clear search button.
  $('.clear-search-button').show();

  // Hide error message.
  $('.no-search-results').hide();

  // Nearme is not active anymore.
  $('.near-me').removeClass('active');

  // Get search variables.
  var searchstr = $('.search-input').val();
  var address = encodeURI(searchstr);
  var geocodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + googleApiKey;

  // Reverse Geocoding the Search
  $.ajax({
    type: "GET",
    url: geocodeUrl,
  }).done(function(data) {
    // Get the coords.
    if (data['status'] != 'OK') {
      $('.no-search-results').show();
      return;
    }
    lat = data['results'][0]['geometry']['location']['lat'];
    long = data['results'][0]['geometry']['location']['lng'];

    // Quick validation.
    if (lat != "" && lat != undefined) {
      // Clear other branches:
      branches = '';
      coop_branches = '';
      setMapOnAll();

      //
      // Load GeoBranches
      //
      // loadGeoBranches(lat, long);
      let requests = loadGeoBranches(lat, long);
      addLoader(container, requests);
      Promise.allSettled(requests).then(function(values){
        for(let i = 0; i < values.length; i++) {
          if(values[i].status === "fulfilled") {
            values[i].value.cb(values[i].value.data);
          }
          
        }
        getBranchesByType();
        container.classList.remove('loading');
        container.classList.add('loaded');
      });

      // Center map
      centerMapSearchLocation(lat, long);
    }
  });
}

function clearSearchBranches(event) {
  event.preventDefault();

  // Reset Search Str
  $('.search-input').val('');

  // Searching OFF
  searching = 0;

  // Show search button.
  $('.search-button').show();

  // Hide clear search button.
  $('.clear-search-button').hide();

  // Hide error message.
  $('.no-search-results').hide();

  // Nearme is not active anymore.
  $('.near-me').removeClass('active');

  if (myLat != '') {
    lat = myLat;
    long = myLong;
    setMarkerMyLocation(lat, long);

    // Nearme is not active anymore.
    $('.near-me').addClass('active');
  } else {
    lat = starting_lat;
    long = starting_lng;
    // Center map on default location
    centerMapSearchLocation(lat, long);
  }

  //
  // Load GeoBranches
  //
  // loadGeoBranches(lat, long);
  let requests = loadGeoBranches(lat, long);
  addLoader(container, requests);
  Promise.allSettled(requests).then(function(values){
    for(let i = 0; i < values.length; i++) {
      if(values[i].status === "fulfilled") {
        values[i].value.cb(values[i].value.data);
      }
      
    }
    getBranchesByType();
    container.classList.remove('loading');
    container.classList.add('loaded');
  });
}

// Nearme Function (Only Focus on your location if any).
function nearMe(elem) {

  // If My Location already active, Deaactivate it.
  if ($(elem).hasClass('active')) {

    // Reset the active class.
    $(elem).removeClass('active');

    // Reset myLat
    myLat = '';
    myLong = '';

    //
    // Load GeoBranches
    //
    //loadGeoBranches(starting_lat, starting_lng);
    let requests = loadGeoBranches(starting_lat, starting_lng);
    addLoader(container, requests);
    Promise.allSettled(requests).then(function(values){
      for(let i = 0; i < values.length; i++) {
        if(values[i].status === "fulfilled") {
          values[i].value.cb(values[i].value.data);
        }
        
      }
      getBranchesByType();
      container.classList.remove('loading');
      container.classList.add('loaded');
    });

    // Center map on default location
    centerMapSearchLocation(starting_lat, starting_lng);

  } else { // Turn ON my Location

    // Try to get location from device/otherwise use only bloom branches.
    if (navigator.geolocation) {

      // Show loader
      $(elem).addClass('loading');

      navigator.geolocation.getCurrentPosition(function(position) {
          // Clear search if any
          $('.search-input').val('');
          $('.branch-list-container').html('');

          // Hide loader, set active
          $(elem).removeClass('loading');
          $(elem).addClass('active');

          lat = position.coords.latitude;
          long = position.coords.longitude;
          myLat = position.coords.latitude;
          myLong = position.coords.longitude;

          // Load GeoBranches
          // loadGeoBranches(lat, long);
          let requests = loadGeoBranches(lat, long);
          addLoader(container, requests);
          Promise.allSettled(requests).then(function(values){
            for(let i = 0; i < values.length; i++) {
              if(values[i].status === "fulfilled") {
                values[i].value.cb(values[i].value.data);
              }
              
            }
            getBranchesByType();
            container.classList.remove('loading');
            container.classList.add('loaded');
          });
          // My Location Markers
          setMarkerMyLocation(lat, long);

        },
        function(error) {
          // ELSE if getlocation not active // DEFAULT to Company Location

          // Clear last Search
          $('.branch-list-container').html('');

          lat = starting_lat;
          long = starting_lng;

          // Hide loader
          $(elem).removeClass('loading');

          // Display information about error
          displayLocationErrorInfo(error);

          // Load GeoBranches
          // loadGeoBranches(starting_lat, starting_lng);
          let requests = loadGeoBranches(starting_lat, starting_lng);
          addLoader(container, requests);
          Promise.allSettled(requests).then(function(values){
            for(let i = 0; i < values.length; i++) {
              if(values[i].status === "fulfilled") {
                values[i].value.cb(values[i].value.data);
              }
              
            }
            getBranchesByType();
            container.classList.remove('loading');
            container.classList.add('loaded');
          });

          // Center map on default location
          myLocation = 0;
          centerMapSearchLocation(starting_lat, starting_lng);
        });
    } // END if getlocation
  }
}

function setMarkerMyLocation(latitude, longitude) {
  image_me = map_near_me_icon;
  myLatlng = new google.maps.LatLng(latitude, longitude);
  marker = new google.maps.Marker({
    position: myLatlng,
    title: 'My Location',
    icon: image_me,
    zIndex: 999
  });

  marker.setMap(map);
  markers.push(marker);

  map.setCenter(myLatlng);
  map.setZoom(13);
}

function loadGeoBranches(lat, long, zoom) {
  // Set zoom
  if (zoom == null) {
      zoom = 13;
  }
  let requests = [];
  // Clear last Search/Load
  $('.branch-list-container').html('');

  // Clear markers
  setMapOnAll(null);

  // Load CO-OP Branches
  if (enable_coop_branches) {
      requests.push(
        coopBranchPromise(lat, long, zoom)
        // $.ajax({
        //   type: "GET",
        //   url: site_url + '/wp-json/locations/v1/coop?' + 'loctype=S' + '&latitude=' + lat + '&longitude=' + long + '&maxRadius=' + radius_coop,
        // }).done(function(data) {
        //   var json = JSON.parse(data);
        //   coop_branches = json.response.locations;
        //   if(coop_branches.length){
        //     coop_branches.forEach(function(branch) {
        //       console.log(branch.locatorType);
        //       setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
        //       listBranch(branch, branch.locatorType);
        //     });
        //   }
        //   // map.setCenter(myLocationMarker);
        //   map.setZoom(zoom);
        // })
      );
  }
 
  // Load CO-OP ATMs
  if (enable_coop_atms) {
    requests.push(
      new coopATMPromise(lat, long, zoom)
    );
  }

  // Load Plugin Locations
  requests.push(
    new branchLocationPromise(lat, long, zoom)
    );
 
  // // Load Walgreens Locations
  if (enable_walgreens) {
      requests.push(new wagLocationPromise(lat, long, zoom));
  }

  return requests;

}

function getBranchesByType(elem) {
  // Active/deactive element.
  if( elem !== '' ) {
    $(elem).toggleClass('active');
  }

  // Clear branches list Container
  $('.branch-list-container').html('');

  // Clear markers
  setMapOnAll(null);

  // Default marker states
  var lastMarker = '';
  markers = [];

  // Start Plugin Locations Filter Engine
  if ($('.nav-filter.branch').hasClass('active') && $('.nav-filter.atm').hasClass('active')) {
    bloom_type = 'both';
  } else if ($('.nav-filter.branch').hasClass('active')) {
    bloom_type = 'Branch';
  } else if ($('.nav-filter.atm').hasClass('active')) {
    bloom_type = 'ATM';
  } else {
    bloom_type = 'none';
  }

  // Start COOP Locations Filters Engine
  var coop_filters = ['.nav-filter.shared-express', '.nav-filter.shared-branch', '.nav-filter.shared-atm'];
  var coop_active = 0;
  var coop_active_filters = "";
  coop_filters.forEach(function(filter) {
    if ($(filter).hasClass('active')) {
      // if (filter === '.nav-filter.shared-express') {
      //   coop_active_filters = 'SE';
      // }
      if (filter === '.nav-filter.shared-branch') {
        coop_active_filters = coop_active_filters + '-S';
      }
      if (filter === '.nav-filter.shared-atm') {
        coop_active_filters = coop_active_filters + '-A';
      }
      coop_active++;
    }
  });
  // Coop filter engine results
  coop_type = coop_active_filters;

  // Start Walgreens Locations Filter Engine
  var walgreens = false;
  if ($('.nav-filter.walgreens').hasClass('active')) {
    walgreens = true;
  }

  // If my location is saved
  // Draw my location Marker (and recenter map to my location)
  if (myLat != '' && searching == 0) {
    setMarkerMyLocation(myLat, myLong);
  } else if (searching == 1) {
    centerMapSearchLocation(lat, long);
  } else {
    centerMapSearchLocation(starting_lat, starting_lng);
  }

  // Draw marker for every Plugin Location
  if (enable_branches && typeof branches !== 'undefined' && branches.length) {
    branches.forEach(function(branch) {
      if (branch.google_map.lat != null && (branch.type == bloom_type || bloom_type == 'both')) {
        setMarker(branch.google_map.lat, branch.google_map.lng, branch.title, branch.type, branch);
        listBranch(branch, branch.type);
      }
    });
  }

  // Draw marker for every COOP API Branch
  if (enable_coop_branches && typeof coop_branches !== 'undefined' && coop_branches.length) {
    coop_branches.forEach(function(branch) {
      // Set the markers
      if (branch.latitude != null) {

        if (coop_type == "SE") {
          if (branch.locatorType == "S" && branch.selfServiceOnly == "Y") {
            setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
            listBranch(branch, branch.locatorType);
          }
        }

        if (coop_type == "SE-S") {
          if (branch.locatorType == "S" && branch.selfServiceOnly == "Y" || branch.locatorType == "AS" || branch.locatorType == "S") {
            setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
            listBranch(branch, branch.locatorType);
          }
        }

        if (coop_type == "SE-A") {
          if (branch.locatorType == "S" && branch.selfServiceOnly == "Y" || branch.locatorType == "A") {
            setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
            listBranch(branch, branch.locatorType);
          }
        }

        if (coop_type == "-S") {
          if (branch.locatorType == "S") {
            setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
            listBranch(branch, branch.locatorType);
          }
        }

        if (coop_type == "-S-A") {
          if (branch.locatorType == "S" || branch.locatorType == "A") {
            setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
            listBranch(branch, branch.locatorType);
          }
        }

        if (coop_type == "-A") {
          if (branch.locatorType === "A") {
            setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
            listBranch(branch, branch.locatorType);
          }
        }

      }
    });
  }
  if (enable_coop_atms && typeof coop_atms !== 'undefined' && coop_atms.length) {
    // Draw marker for every Coop ATM
    coop_atms.forEach(function(branch) {
      // Set the markers
      if (branch.latitude != null) {

          if (coop_type == "SE") {
            if (branch.locatorType == "S" && branch.selfServiceOnly == "Y") {
              setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
              listBranch(branch, branch.locatorType);
            }
          }

          if (coop_type == "SE-S") {
            if (branch.locatorType == "S" && branch.selfServiceOnly == "Y" || branch.locatorType == "AS" || branch.locatorType == "S") {
              setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
              listBranch(branch, branch.locatorType);
            }
          }

          if (coop_type == "SE-A") {
            if (branch.locatorType == "S" && branch.selfServiceOnly == "Y" || branch.locatorType == "A") {
              setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
              listBranch(branch, branch.locatorType);
            }
          }

          if (coop_type == "-S") {
            if (branch.locatorType == "S") {
              setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
              listBranch(branch, branch.locatorType);
            }
          }

          if (coop_type == "-S-A") {
            if (branch.locatorType == "S" || branch.locatorType == "A") {
              setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
              listBranch(branch, branch.locatorType);
            }
          }

          if (coop_type == "-A") {
            if (branch.locatorType === "A") {
              setMarkerCoop(branch.latitude, branch.longitude, branch.institutionName, branch.locatorType, branch.selfServiceOnly, branch);
              listBranch(branch, branch.locatorType);
            }
          }

      }
    });
  }

  // Draw marker for every Walgreens Location
  if (walgreens) {
      walgreensLocations.forEach(function(walgreensLocation) {
        wag_name = getWalgreensLocationName(walgreensLocation);
        setMarker(walgreensLocation.latitude, walgreensLocation.longitude, wag_name, 'Walgreens', walgreensLocation);
        listBranch(walgreensLocation, 'Walgreens');
      });
  }

  // map.setCenter(lastMarker);
  // map.setZoom(11);
}

function getWalgreensLocationName(walgreensLocation) {
  let wag_name = '';
  if(typeof walgreensLocation.store.locationName !== 'undefined') {
    wag_name = walgreensLocation.store.locationName;
  } else {
    wag_name = walgreensLocation.store.brand + ' #' + walgreensLocation.store.storeNumber;
  }
  return wag_name
}

function setMarker(latitude, longitude, name, type, branch) {

  // Draw icons depending on branch/atm type.
  // type 0 = Bloom Branch
  // type 1 = Bloom ATM
  if (type == 0 || type == 'Branch') {
    image = map_our_branches_icon;
  } else if (type == 1 || type == 'ATM') {
    image = map_our_atms_icon;
  } else if (type == 'Walgreens') {
    image = map_walgreens_icon;
  }

  // Set the marker.
  if (latitude != null && longitude != null) {
    myLatlng = new google.maps.LatLng(latitude, longitude);

    if (type == 'Branch') {
      marker = new google.maps.Marker({
        position: myLatlng,
        title: name,
        icon: image,
        zIndex: 997
      });
    } else if (type == 'ATM') {
      // Set z-index of ATM pins to be greater than branches
      marker = new google.maps.Marker({
        position: myLatlng,
        title: name,
        icon: image,
        zIndex: 998
      });
    } else if (type == 'Walgreens') {
	  marker = new google.maps.Marker({
        position: myLatlng,
        title: name,
        icon: image,
        zIndex: 996
      });
	}

	if (type == 'Walgreens') {
		// On Click (Show in left panel)
		google.maps.event.addListener(marker, 'click', (function(marker) {
		  return function() {
			  openBranchInfo(branch, 'Walgreens');
		  }
		})(marker));
	} else {
		// On Click (Show in left panel)
		google.maps.event.addListener(marker, 'click', (function(marker) {
		  return function() {
			openBranchInfo(branch, 'plugin');
		  }
		})(marker));
	}

    lastMarker = myLatlng;
    marker.setMap(map);
    markers.push(marker);
  }
}

function setMarkerCoop(latitude, longitude, name, coop_type, selfservice, branch) {
  // Draw circles depending on branch type.
  // Shared Express
 
  if (coop_type === "S" && selfservice === "Y") {
    image = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: '#48515C',
      fillOpacity: 1,
      strokeColor: '#fff',
      strokeOpacity: 1,
      strokeWeight: 2,
      scale: 10
    };
    // Shared Branch
  } else if (coop_type === "S" || coop_type === "AS") {
    image = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: map_coop_branches_color,
      fillOpacity: 1,
      strokeColor: '#fff',
      strokeOpacity: 1,
      strokeWeight: 2,
      scale: 10
    };
    // Shared ATM
  } else if (coop_type === "A") {
    image = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: map_coop_atms_color,
      fillOpacity: 1,
      strokeColor: '#fff',
      strokeOpacity: 1,
      strokeWeight: 2,
      scale: 10
    };
  } else {
    image = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: '#f00',
      fillOpacity: 1,
      strokeColor: '#fff',
      strokeOpacity: 1,
      strokeWeight: 2,
      scale: 10
    };
  }

  // Set the marker.
  if (latitude != null && longitude != null) {
    myLatlng = new google.maps.LatLng(latitude, longitude);
    marker = new google.maps.Marker({
      position: myLatlng,
      title: name,
      icon: image
    });

    // On Mouseover
    google.maps.event.addListener(marker, 'mouseover', (function(marker) {
      return function() {
        infowindow.setContent(name);
        infowindow.setOptions({
          maxWidth: 200
        });
        infowindow.open(map, marker);
      }
    })(marker));

    // On Mouseout
    marker.addListener('mouseout', function() {
      infowindow.close();
    });

    // On Click (Show in left panel)
    google.maps.event.addListener(marker, 'click', (function(marker) {
      return function() {
        openBranchInfo(branch, 'coop');
        // replaceInInfo(); // Function set on client for cleaning info from co-op api
      }
    })(marker));

    lastMarker = myLatlng;
    // To add the marker to the map, call setMap();
    marker.setMap(map);
    markers.push(marker);
  }
}

function centerMapSearchLocation(latitude, longitude) {
  myLatlng = new google.maps.LatLng(latitude, longitude);
  myLocationMarker = myLatlng;
  map.setCenter(myLatlng);
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

/*
 * List Branch
 * -----------
 * Renders location item in left panel
 *
 */
function listBranch(branch, type) {
  var image = '',
    name = '',
    subtitle = '',
    distance = '',
    source = '',
    access = '',
    label_prefix = '';

  // Type is COOP Branch
  if (type == 'S') {
    label_prefix = 'COOP Branch '+branch.id;
    image = filter_coop_branches_icon;
    name = branch.institutionName;
    distance = branch.distance;
    source = 'coop';
    // console.log(branch);

    // Type is COOP ATM
  } else if (type == 'A') {
    label_prefix = 'COOP ATM '+branch.id;
    image = filter_coop_atms_icon;
    name = branch.institutionName;
    distance = branch.distance;
    source = 'coop';

    if (branch.hours_open_value == "24 HOURS ACCESS" || branch.open24Hours == "Y") {
      access = "24/7 "
    }

    if (branch.installationType == "Walk-Up") {
      access += "Walk-Up ";
    }

    // Type is Plugin Branch
    // Only expect subtitle from plugin locations
  } else if (type == 'Branch') {
    label_prefix = 'CU Branch';
    image = filter_our_branches_icon;
    name = branch.title;
    subtitle = (branch.sub_title !== null) ? branch.sub_title : '';
    distance = branch.distance;
    source = 'plugin';

    // Type is Plugin ATM
    // Only expect subtitle from plugin locations
  } else if (type == 'ATM') {
    label_prefix = 'CU ATM';
    image = filter_our_atms_icon;
    name = branch.title;
    subtitle = (branch.sub_title !== null && typeof branch.sub_title !== 'undefined') ? branch.sub_title : '';
    distance = branch.distance;
    source = 'plugin';

    // Type is Walgreens
  } else if (type == 'Walgreens') {
      label_prefix = 'Walgreens';
	    image = filter_walgreens_icon;
      if(typeof branch.store.locationName !== 'undefined') {
        name = branch.store.locationName;
      } else {
        name = branch.store.brand + ' #' + branch.store.storeNumber;
      }
      
      distance = branch.distance;
      source = 'Walgreens';
  }

  // Create element
  var el = $([
    '<button class="branch-list-single" aria-label="' + label_prefix + ': ' + name + '">',
        '<div class="branch-list-left">',
            '<img src="' + image + '" role="presentation" class="pin" alt="">',
        '</div>',
        '<div class="branch-list-right">',
            '<div class="branch-list-distance">' +
                distance + ' mi',
            '</div>',
            '<div class="branch-list-title">' +
                name +
            '</div>',
            '<div class="branch-list-subtitle">',
                '<small>' +
                    subtitle +
                '</small>',
            '</div>',
        '</div>',
    '</button>'
  ].join('\n'));

  // Attach openBranchInfo() click event to the element
  el.click(function() {
    openBranchInfo(branch, source);
  });

  // Append element to branch list
  $('.branch-list-container').append(el);
}

/*
 * Open Branch Info
 * ----------------
 * Renders branch information in left panel modal
 *
 */
function openBranchInfo(branch, source) {
  $('.branch-info-container').show();
  $('.nav-container').hide();

  // Reset Hidden Containers if Any.
  $('.branch-hours-container').show();

  // Main object
  var branch_title = "No info";
  var branch_subtitle = "No info";
  var branch_address = "No info";
  var branch_address2 = "No info";
  var branch_phone = "No info";
  var branch_direction = "No info";
  var branch_monday = "Unavailable";
  var branch_tuesday = "Unavailable";
  var branch_wednesday = "Unavailable";
  var branch_thursday = "Unavailable";
  var branch_friday = "Unavailable";
  var branch_saturday = "Unavailable";
  var branch_sunday = "Unavailable";
  var branch_drive_monday = "Unavailable";
  var branch_drive_tuesday = "Unavailable";
  var branch_drive_wednesday = "Unavailable";
  var branch_drive_thursday = "Unavailable";
  var branch_drive_friday = "Unavailable";
  var branch_drive_saturday = "Unavailable";
  var branch_drive_sunday = "Unavailable";
  var branch_phone = "Unavailable";
  var branch_phone_list = ""
  var branch_website = "No info";
  var branch_services = "";

  /*
   * ---------------------------------------------------------------------------
   * Source: COOP
   * ---------------------------------------------------------------------------
   */
  if (source === "coop") {
    // Show
    $('.branch-subtitle').show();
    $('.branch-phone').show();

    // Hide
    $('.branch-phone-numbers-container').hide();

    // If COOP ATM, hide these
    if (branch.locatorType == "A") {
      $('.branch-subtitle').hide();
      $('.branch-phone').hide();
    }
    
    branch_title = branch.institutionName;
    branch_subtitle = '';
    branch_address = branch.address;
    
    branch_address2 = branch.city + ', ' + branch.state + ' ' + branch.zip;
    branch_phone = branch.phone;
    branch_direction = '<a href="https://www.google.com/maps/?q=' + branch.institutionName + ' ' + branch.address + ' ' + branch.state + ' ' + branch.zip + '" target="_blank">'+locationsLocal.get_directions+'</a>';
    additional_links = branch.additional_links;

    // Branch Hours
    if (branch.locatorType == "A") {
      // Hide if ATM
      $('.branch-hours-container').hide();
    } else {
      // Show in case hidden
      $('.branch-hours-container').show();
      branch_monday = getOpenClosed(branch.mondayOpen, branch.mondayClose);
      branch_tuesday = getOpenClosed(branch.tuesdayOpen, branch.tuesdayClose);
      branch_wednesday = getOpenClosed(branch.wednesdayOpen, branch.wednesdayClose);
      branch_thursday = getOpenClosed(branch.thursdayOpen, branch.thursdayClose);
      branch_friday = getOpenClosed(branch.fridayOpen, branch.fridayClose);
      branch_saturday = getOpenClosed(branch.saturdayOpen, branch.saturdayClose);
      branch_sunday = getOpenClosed(branch.sundayOpen, branch.sundayClose);
    }

    // Drive-Thru Hours
    if (branch.driveThru == "N" || branch.mondayDriveThruOpen == "") {
      // Hide if no drive-thru
      $('.branch-drive-thru-hours-container').hide();
    } else {
      // Show in case hidden
      $('.branch-drive-thru-hours-container').show();
      branch_drive_monday = getOpenClosed(branch.mondayDriveThruOpen, branch.mondayDriveThruClose);
      branch_drive_tuesday = getOpenClosed(branch.tuesdayDriveThruOpen, branch.tuesdayDriveThruClose);
      branch_drive_wednesday = getOpenClosed(branch.wednesdayDriveThruOpen, branch.wednesdayDriveThruClose);
      branch_drive_thursday = getOpenClosed(branch.thursdayDriveThruOpen, branch.thursdayDriveThruClose);
      branch_drive_friday = getOpenClosed(branch.fridayDriveThruOpen, branch.fridayDriveThruClose);
      branch_drive_saturday = getOpenClosed(branch.saturdayDriveThruOpen, branch.saturdayDriveThruClose);
      branch_drive_sunday = getOpenClosed(branch.sundayDriveThruOpen, branch.sundayDriveThruClose);
    }

    // Phone
    if (branch.phone == "") {
      // Hide if no phone
      $('.branch-phone').hide();
    } else {
      // Show in case hidden
      $('.branch-phone').show();
      branch_phone = '<a href="tel:' + branch.phone + '" target="_blank">' + formatPhoneNumber(branch.phone) + '</a>';
    }

    // Website
    $('.branch-websites-container').hide();
    // if (branch.webAddress == "") {
    //     // Hide if no website
    //     $('.branch-websites-container').hide();
    // } else {
    //     // Show in case hidden
    //     $('.branch-websites-container').show();
    //     branch_website = '<a href="https://'+branch.webAddress+'" target="_blank">'+branch.webAddress+'</a>';
    // }

    // Services
    if (branch.open24Hours) {
      $('.branch-services-container').show();

      if (branch.open24Hours) {
        branch_services = '<tr><td>'+locationsLocal.twenty_four_hours+':</td><td>' + branch.open24Hours + '</td></tr>';
      }

      if (branch.selfServiceDevice) {
        branch_services += '<tr><td>'+locationsLocal.self_service+':</td><td>' + branch.selfServiceDevice + '</td></tr>';
      }

      if (branch.handicapAccess) {
        branch_services += '<tr><td>'+locationsLocal.handicap_access+':</td><td>' + branch.handicapAccess + '</td></tr>';
      }

      if (branch.acceptCash) {
        branch_services += '<tr><td>'+locationsLocal.cash+':</td><td>' + branch.acceptCash + '</td></tr>';
      }

      if (branch.acceptDeposit) {
        branch_services += '<tr><td>'+locationsLocal.deposit+':</td><td>' + branch.acceptDeposit + '</td></tr>';
      }
    }

  }

  /*
   * ---------------------------------------------------------------------------
   * Source: Plugin
   * ---------------------------------------------------------------------------
   */
  if (source === "plugin") {
    // Show
    
    $('.branch-subtitle').show();
    $('.branch-phone').show();
    $('.branch-phone-numbers-container').show();

    // Hide
    $('.branch-websites-container').hide();

    // If plugin ATM, hide these
    if (branch.type == "ATM") {
      // $('.branch-hours-container').hide();
      $('.branch-phone').hide();
      $('.branch-phone-numbers-container').hide();
    }
    branch_photo = branch.photo;
    branch_phone = branch.main_phone;
    branch_additional_numbers = branch.branch_additional_numbers;
    has_no_main_number = branch.main_phone === 'undefined' || branch.main_phone === '';

    if (has_no_main_number) {
      $('.branch-phone-numbers-container').hide();
    }
    branch_title = branch.title;
    branch_subtitle = branch.sub_title;
    
    // Check for street_address_line_2
    if (branch.street_address_line_2 != '') {
      branch_address = branch.street_address_line_1 + '<br>' + branch.street_address_line_2;
    } else {
      branch_address = branch.street_address_line_1;
    }

    branch_address2 = branch.city + ', ' + branch.state + ' ' + branch.zip;
    
    branch_direction = '<a href="https://www.google.com/maps/?q=' + branch.title + ' ' + branch.street_address_line_1 + ' ' + branch.city + ' ' + branch.state + ' ' + branch.zip + '" target="_blank">'+locationsLocal.get_directions+'</a>';

    if( ! branch.hours ) {
      branch.hours = [];
    }
    var branch_hours_list = Object.keys(branch.hours).filter(function (itm) { return branch.hours[itm] !== ''});

    if( ! branch.drive_thru_hours ) {
      branch.drive_thru_hours = [];
    }
    var branch_dt_hours_list = Object.keys(branch.drive_thru_hours).filter(function (itm) { return branch.drive_thru_hours[itm] !== '' } );

    if ( branch_hours_list.length < 1 ) {

      // Hide if no drive-thru
      $('.branch-hours-container').hide();
    } else {
      // Show in case hidden
      $('.branch-drive-thru-hours-container').show();
      branch_monday = getOpenClosed( branch.hours.monday_open, branch.hours.monday_close );
      branch_tuesday = getOpenClosed( branch.hours.tuesday_open, branch.hours.tuesday_close );
      branch_wednesday = getOpenClosed( branch.hours.wednesday_open, branch.hours.wednesday_close );
      branch_thursday = getOpenClosed( branch.hours.thursday_open, branch.hours.thursday_close );
      branch_friday = getOpenClosed( branch.hours.friday_open, branch.hours.friday_close );
      branch_saturday = getOpenClosed( branch.hours.saturday_open, branch.hours.saturday_close );
      branch_sunday = getOpenClosed( branch.hours.sunday_open, branch.hours.sunday_close );
    }

    // Drive-Thru Hours
    if ( branch_dt_hours_list.length < 1 ) {
      // Hide if no drive-thru
      $('.branch-drive-thru-hours-container').hide();
    } else {
      // Show in case hidden
      $('.branch-drive-thru-hours-container').show();
      branch_drive_monday = getOpenClosed( branch.drive_thru_hours.monday_open, branch.drive_thru_hours.monday_close );
      branch_drive_tuesday = getOpenClosed( branch.drive_thru_hours.tuesday_open, branch.drive_thru_hours.tuesday_close );
      branch_drive_wednesday = getOpenClosed( branch.drive_thru_hours.wednesday_open, branch.drive_thru_hours.wednesday_close );
      branch_drive_thursday = getOpenClosed( branch.drive_thru_hours.thursday_open, branch.drive_thru_hours.thursday_close );
      branch_drive_friday = getOpenClosed( branch.drive_thru_hours.friday_open, branch.drive_thru_hours.friday_close );
      branch_drive_saturday = getOpenClosed( branch.drive_thru_hours.saturday_open, branch.drive_thru_hours.saturday_close );
      branch_drive_sunday = getOpenClosed( branch.drive_thru_hours.sunday_open, branch.drive_thru_hours.sunday_close );
    }
    if (branch.photo) {
      
      $('.branch-photo-wrap').html('<div class="h-cover-bg branch-photo" style="background-image:url('+branch.photo+');"></div>')
    }
    // Phone
    if ( !branch.main_phone ) {
      // Hide if no phone
      $('.branch-phone').hide();
    } else {
      // Show in case hidden
      $('.branch-phone').show();

      // Main phone number
      branch_phone = '<a href="tel:' + branch.main_phone + '" target="_blank">' + formatPhoneNumber(branch.main_phone) + '</a>';

      branch_phone_list = '<tr><td>Main Phone</td><td>' + branch_phone + '</a></td></tr>';

      //Creat list of phone numbers
      var has_additional_numbers = branch.additional_numbers && branch.additional_numbers.length;
      if(has_additional_numbers){
        branch.additional_numbers.forEach(function(phone, i) {
          branch_phone_list += '<tr><td>' + branch.additional_numbers[i]["name"] + '</td><td><a href="tel:' + branch.additional_numbers[i]["number"] + '" class="branch-phone">' + formatPhoneNumber(branch.additional_numbers[i]["number"]) + '</a></td></tr>'
        });
      }

    }

    // Services
    if (branch.type == 'ATM') {
      // If BloomCU ATM
      $('.branch-services-container').hide();
    } else {
      // If BloomCU Branch
      $('.branch-services-container').hide();
    }

  }

  /*
   * ---------------------------------------------------------------------------
   * Source: Walgreens
   * ---------------------------------------------------------------------------
   */
  if (source === "Walgreens") {
    
    // Show
    $('.branch-subtitle').show();
    $('.branch-phone').show();
    $('.branch-phone-numbers-container').show();
    $('.branch-drive-thru-hours-container').hide();
    $('.branch-services-container').hide();

    // Hide
    $('.branch-websites-container').hide();

    // If plugin ATM, hide these
    if (branch.type == "ATM") {
      // $('.branch-hours-container').hide();
      $('.branch-phone').hide();
      $('.branch-phone-numbers-container').hide();
    }

    branch_title = getWalgreensLocationName(branch);
    
    branch_subtitle = '';
    
  	branch_address = branch.store.address.street;
    branch_address2 = branch.store.address.city + ', ' + branch.store.address.state + ' ' + branch.store.address.zip;
    branch_phone = branch.stph;
    branch_direction = '<a href="https://www.google.com/maps/?q=' + branch_title + ' ' + branch.store.address.street + ' ' + branch.store.address.city + ' ' + branch.store.address.state + ' ' + branch.store.address.zip + '" target="_blank">'+locationsLocal.get_directions+'</a>';

	  branch_monday = getOpenClosed( branch.store.storeOpenTime, branch.store.storeCloseTime );
    branch_tuesday = getOpenClosed( branch.store.storeOpenTime, branch.store.storeCloseTime );
    branch_wednesday = getOpenClosed( branch.store.storeOpenTime, branch.store.storeCloseTime );
    branch_thursday = getOpenClosed( branch.store.storeOpenTime, branch.store.storeCloseTime );
    branch_friday = getOpenClosed( branch.store.storeOpenTime, branch.store.storeCloseTime );
    branch_saturday = getOpenClosed( branch.store.storeOpenTime, branch.store.storeCloseTime );
    branch_sunday = getOpenClosed( branch.store.storeOpenTime, branch.store.storeCloseTime );

    // Phone
    if ( !branch.store.phone ) {
      // Hide if no phone
      $('.branch-phone').hide();
    } else {
      // Show in case hidden
      $('.branch-phone').show();
      branch.stph = branch.store.phone.areaCode + branch.store.phone.number;
      // Main phone number
      branch_phone = '<a href="tel:' + branch.stph + '" target="_blank">' + formatPhoneNumber(branch.stph) + '</a>';

      branch_phone_list = '<tr><td>Main Phone</td><td>' + branch_phone + '</a></td></tr>';

      //Creat list of phone numbers
      /*var has_additional_numbers = branch.additional_numbers && branch.additional_numbers.length;
      if(has_additional_numbers){
        branch.additional_numbers.forEach(function(phone, i) {
          branch_phone_list += '<tr><td>' + branch.additional_numbers[i]["name"] + '</td><td><a href="tel:' + branch.additional_numbers[i]["number"] + '" class="branch-phone">' + formatPhoneNumber(branch.additional_numbers[i]["number"]) + '</a></td></tr>'
        });
      }*/

    }

    // Services
    /*if (branch.type == 'ATM') {
      // If BloomCU ATM
      $('.branch-services-container').hide();
    } else {
      // If BloomCU Branch
      $('.branch-services-container').hide();
    }*/

  }

  // Update the current text/html
  $('.branch-title').html(branch_title);
  $('.branch-subtitle').html(branch_subtitle);
  $('.branch-address').html(branch_address);
  $('.branch-address2').html(branch_address2);
  $('.branch-phone').html(branch_phone);
  $('.branch-direction').html(branch_direction);
  if(branch.additional_links) {
    for(var i = 0; i<branch.additional_links.length; i++) {
      $('.branch-direction').append(`<a href="${branch.additional_links[i].link.url}">${branch.additional_links[i].link.title}</a>`);
    }
    
  }
  
  
  $('.branch-monday').html(branch_monday);
  $('.branch-tuesday').html(branch_tuesday);
  $('.branch-wednesday').html(branch_wednesday);
  $('.branch-thursday').html(branch_thursday);
  $('.branch-friday').html(branch_friday);
  $('.branch-saturday').html(branch_saturday);
  $('.branch-sunday').html(branch_sunday);
  $('.branch-drive-monday').html(branch_drive_monday);
  $('.branch-drive-tuesday').html(branch_drive_tuesday);
  $('.branch-drive-wednesday').html(branch_drive_wednesday);
  $('.branch-drive-thursday').html(branch_drive_thursday);
  $('.branch-drive-friday').html(branch_drive_friday);
  $('.branch-drive-saturday').html(branch_drive_saturday);
  $('.branch-drive-sunday').html(branch_drive_sunday);
  $('.branch-phones').html(branch_phone_list);
  $('.branch-website').html(branch_website);
  $('.branch-services').html(branch_services);
}

/*
 * Display Location Error Info
 * ----------------
 * Renders branch information in left panel modal
 *
 */
function displayLocationErrorInfo(error) {
  var title,
    subtitle,
    body = 'Please <a href="https://www.lifewire.com/denying-access-to-your-location-4027789" target="black">click here</a> to learn how to enable location services in your browser. If you continue experiencing this, please check your internet connection and the location settings on your device, then try again.';

  // Which error
  switch (error.code) {
    case error.PERMISSION_DENIED:
      title = 'Permission denied';
      subtitle = 'Our request for your location was denied by your browser.';
      break;

    case error.POSITION_UNAVAILABLE:
      title = 'Position unavailable';
      subtitle = 'Your position is unavailable.';
      break;

    case error.TIMEOUT:
      title = 'Took too long';
      subtitle = 'The request for your location took too long.';
      break;

    case error.UNKNOWN_ERROR:
      title = 'Unknown location error';
      subtitle = 'Something went wrong while requesting your location.';
      break;
  }

  // Hide nav container
  $('.nav-container').hide();

  // Populate error info
  $('.other-info-title').html(title);
  $('.other-info-subtitle').html(subtitle);
  $('.other-info-body').html(body);

  // Show container
  $('.other-info-container').show();
}

/*
 * Open/Close Info Containers
 * ----------------
 * Renders branch information in left panel modal
 *
 */
// Branch Info Container
// Holds information about a single location
function closeBranchInfo() {
  $('.branch-info-container').hide();
  $('.nav-container').show();
}

// Other Info Container
// Holds any information appended to it we need to show the user
// E.g., information about their browser, or "No locations found".
function closeOtherInfo() {
  $('.other-info-container').hide();
  $('.nav-container').show();
}

/*
 * Format Phone Number
 * ----------------
 * Formats '11234567890' to '+1 (123) 456-7890'
 *
 */
function formatPhoneNumber(phoneNumberString) {
  var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    var intlCode = (match[1] ? '+1 ' : '')
    return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
  }
  return null
}

/*
 * Get Open or Closed
 * ----------------
 * Checks if location is closed and returns "Closed", else returns whole hours.
 *
 */
function getOpenClosed(openTime, closeTime) {
  let hasNoOpenTime = openTime == 'Closed' || openTime == undefined || openTime == '';
  let hasNoClosedTime = closeTime == 'Closed' || closeTime == undefined || closeTime == '';

  if (hasNoOpenTime) {
    return locationsLocal.closed;
  } else {
    let ret = openTime;
    if ( ! hasNoOpenTime && ! hasNoClosedTime) {
      ret += ' to ';
    }
    ret += closeTime;
    return  ret;
  }
}

/*
 * Get Time
 * ----------------
 * Converts 24 hour time to 12 hour time with AM/PM
 *
 */
function getTime(time) {
  if (time == null) {
    return undefined; // No data provided
  } else {
    var timeArray = time.split(":");
    var HH = parseInt(timeArray[0]);
    var min = timeArray[1];
    var AMPM = HH >= 12 ? "PM" : "AM";
    var hours;

    if (HH == 0) {
      hours = HH + 12;
    } else if (HH > 12) {
      hours = HH - 12;
    } else {
      hours = HH;
    }

    return hours + ":" + min + " " + AMPM;
  }
}

/*
 * Get Time
 * ----------------
 * Converts time from API to "open until" string
 *
 */
function getOpenUntil(time) {
  var time = getTime(time);
  return 'Open until ' + time;
}


/**
		 * Modify Locate App Filters
		 *
		 * When the user searches, set all filters to active
		 */
 $( document ).ready(function() {

  function resetAllFilters() {
    $('.nav-filter.branch').addClass('active');
    $('.nav-filter.atm').addClass('active');
    $('.nav-filter.shared-branch').addClass('active');
   //  $('.nav-filter.shared-atm').addClass('active');
  }

  $("form.search-form").on( "submit", function( event ) {
    resetAllFilters();
  });

  $(".near-me").click(function( event ) {
    resetAllFilters();
  });
  

});

// Expose maps functions 
window.initMap = initMap.bind(this);
window.closeBranchInfo = closeBranchInfo.bind(this);
window.searchBranches = searchBranches.bind(this);
window.clearSearchBranches = clearSearchBranches.bind(this);
window.nearMe = nearMe.bind(this);
window.getBranchesByType = getBranchesByType.bind(this);
window.closeOtherInfo = closeOtherInfo.bind(this);