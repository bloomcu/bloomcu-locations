<?php
namespace Locations;

/**
 * REST_API Handler
 */
class REST_API {

	public function __construct() {

		add_action( 'rest_api_init', function() {

			// Route: All locations
			register_rest_route( 'locations/v1', '/all', [
				'methods'  => 'GET',
				'callback' => [ $this, 'get_all_locations' ],
			] );

			// Route: Nearby locations
			register_rest_route( 'locations/v1', '/nearby', [
				'methods'  => 'GET',
				'callback' => [ $this, 'get_nearby_locations' ],
			] );

			// Route: Single location
			register_rest_route( 'locations/v1', '/single/(?P<id>\d+)', [
				'methods'  => 'GET',
				'callback' => [ $this, 'get_single_location' ],
			] );

			// Route: COOP API
			register_rest_route( 'locations/v1', '/coop', [
				'methods'  => 'GET',
				'callback' => [ $this, 'get_coop_locations' ],
			] );

			// Route: Walgreens
			register_rest_route( 'locations/v1', '/walgreens', [
				'methods'  => 'GET',
				'callback' => [ $this, 'get_walgreens_locations' ],
			] );

		} );

	}

	/**
	 * Get all locations
	 *
	 */
	public function get_all_locations() {

		// Instance Locations_Plugin class
		global $locationsplugin;

		// Use get_locations method on plugin's Frontend class
		$query = $locationsplugin->frontend->get_all_locations();

		// Associative array to hold our results
	    $results = array();

		// Loop through each post
	    while( $query->have_posts() ) {
	        $query->the_post();

	        // Define location properties, push to $results
	        array_push($results, array(
	            'id' => get_the_ID(),
	            'title' => get_the_title(),
				'sub_title' => get_field( 'sub_title' ),
	            'permalink' => get_the_permalink(),
	            'type' => get_field( 'type' ),
				'photo' => get_field( 'photo' ),
				'street_address_line_1' => get_field( 'street_address_line_1' ),
				'street_address_line_2' => get_field( 'street_address_line_2' ),
				'city' => get_field( 'city' ),
				'state' => get_field( 'state' ),
				'zip' => get_field( 'zip' ),
				'main_phone' => get_field( 'main_phone' ),
				'additional_numbers' => get_field( 'additional_numbers' ),
				'additional_links' => get_field( 'additional_links'),
				'main_email' => get_field( 'main_email' ),
				'google_map' => get_field( 'google_map' ),
				'hours' => get_field( 'hours' ),
				'drive_thru_hours' => get_field( 'drive_thru_hours' ),
	        ));

	    }

		return $results;
	}

	/**
	 * Get nearby locations
	 *
	 */
	public function get_nearby_locations( $params ) {

		// Setup arguments from url parameters
		$args = [
			'latitudeFrom' => $params['latitudeFrom'],
			'longitudeFrom' => $params['longitudeFrom'],
			'search_radius' => $params['search_radius']
		];

		// Instance Locations_Plugin class
		global $locationsplugin;

		// Use get_locations method on plugin's Frontend class
		$query = $locationsplugin->frontend->get_nearby_locations($args);

		// Associative array to hold our results
	    $results = array();

		// Loop over each location
		for ($i = 0; $i <= count($query) - 1; $i++) {

			// Get id and distance
			$id = $query[$i]->ID;
			$distance = $query[$i]->distance;

	        // Define location properties, push to $results
	        array_push($results, array(
	            'id' => $id,
				'distance' => $distance,
	            'title' => get_the_title( $id ),
				'sub_title' => get_field( 'sub_title', $id ),
	            'permalink' => get_the_permalink( $id ),
	            'type' => get_field( 'type', $id ),
				'photo' => get_field( 'photo', $id ),
				'street_address_line_1' => get_field( 'street_address_line_1', $id ),
				'street_address_line_2' => get_field( 'street_address_line_2', $id ),
				'city' => get_field( 'city', $id ),
				'state' => get_field( 'state', $id ),
				'zip' => get_field( 'zip', $id ),
				'main_phone' => get_field( 'main_phone', $id ),
				'additional_numbers' => get_field( 'additional_numbers', $id ),
				'additional_links' => get_field( 'additional_links', $id),
				'main_email' => get_field( 'main_email', $id ),
				'google_map' => get_field( 'google_map', $id ),
				'hours' => get_field( 'hours', $id ),
				'drive_thru_hours' => get_field( 'drive_thru_hours', $id ),
	        ));

	    }

		return $results;
	}

	/**
	 * Get single location
	 *
	 */
	public function get_single_location( $data ) {
		// $locations = get_post( $data['id'] );
		//
		// // Bail if empty or not a locations post
		// if ( empty( $locations ) || $locations->post_type !== 'locations' ) {
		// 	return 0;
		// }
		//
		// // attach locations schema meta
		// $schema = get_post_meta( $data['id'], '_bloom_locations_schema', true );
		// $locations->locations_structure = $schema;
		//
		// // attach locations type
		// $type = get_post_meta( $data['id'], '_bloom_locations_type', true );
		// $locations->locations_type = $type;
		//
		// return $locations;
	}

	/**
	 * Get COOP locations
	 *
	 */
	public function get_coop_locations( $params ) {

		// CO-OP API
		$cpAPIUrl = 'http://api.CO-OPfs.org/locator/proximitySearch?'.'loctype=' . $params['loctype'] . '&latitude=' . $params['latitude'] . '&longitude=' . $params['longitude'].'&maxRadius=' . $params['maxRadius'];
		$cpAPIKey = 'dJNBvuj9yb8pMom';
		$cpRoute = "";

		// JSON and Apikey Headers
		$cpCallHeaders = ["Accept: application/json", "Version: 1", "Authorization: " . $cpAPIKey];

		// Call the API
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $cpAPIUrl . $cpRoute);
		
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // time out after 30 seconds if no co-op does not fully respond
		curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $cpCallHeaders);
		

		// Get the response
		$response = curl_exec($ch);

		// Close cURL connection
		curl_close($ch);
		if($response) {
			return $response;
		} else {
			return [];
		}
		
	}

	/**
	 * Get Walgreens locations
	 *
	 */
	public function get_walgreens_locations( $params ) {
		$lat = $params->get_param('latitudeFrom');
		$lng = $params->get_param('longitudeFrom');
		
		// $apiKey = 'XpkX8pArHIiubfQoK4VftltMcB1p1Q0p';
		$apiKey = 'A32yVj31LNA6wWRPFQtHc5BGVUnJ0oaY';
		// $payload = '{"apiKey": "'.$apiKey.'","affId": "storesapi","lat": "'.$params['latitudeFrom'].'","lng": "'.$params['longitudeFrom'].'","requestType": "locator","act": "fndStore","view": "fndStoreJSON","devinf": "Chrome,26.0", "appver": "1.00"}';
		$payload = '{
			"apiKey": "'.$apiKey.'",
			"affId": "bloomcu",
			"lat": "' . $params['latitudeFrom'] . '",
			"lng": "' . $params['longitudeFrom'] . '",
			"s": "40",
			"p": "1",
			"r": "20",
			"requestType": "locator",
			"appVer": "2.0"
		}';
		$ch = curl_init('https://services.walgreens.com/api/stores/search/v2');
		curl_setopt_array($ch, array(
		    CURLOPT_POST => TRUE,
			CURLOPT_TIMEOUT => 30, // time out after 30 seconds if no Walgreens API does not fully respond
		    CURLOPT_RETURNTRANSFER => TRUE,
		    CURLOPT_HTTPHEADER => array(
		        'Content-Type: application/json'
		    ),
		    CURLOPT_POSTFIELDS => $payload
		));

		// Send the request
		$response = curl_exec($ch);
		
		// curl_close($ch);
		
		// Check for errors


		if($response === FALSE){
			/*
				return an empty array if CURL request fails 
				TODO: handle this more gracefully (add/or an error message or disable Walgreens selection)
			*/
			return [];
		}
		
		$responseData = json_decode($response, TRUE);
		curl_close($ch);
		if(isset($responseData['results'])) {
			return $responseData['results'];
		}
		//return $responseData;
	}

}
