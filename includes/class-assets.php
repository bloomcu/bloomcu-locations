<?php
namespace Locations;

/**
 * Scripts and Styles Class
 */
class Assets {

	public function __construct() {

		if ( is_admin() && function_exists('get_current_screen') ) {
			wp_enqueue_editor();
			add_action( 'admin_enqueue_scripts', [ $this, 'register' ], 5 );
			add_action( 'admin_enqueue_scripts', [ $this, 'localize_admin_script' ], 10 );
		} else {
			add_action( 'wp_enqueue_scripts', [ $this, 'register' ], 5 );
			add_action( 'wp_enqueue_scripts', [ $this, 'localize_frontend_script' ], 10 );
		}

	}

	/**
	 * Register our app scripts and styles
	 *
	 * @return void
	 */
	public function register() {
		$this->register_scripts( $this->get_scripts() );
		$this->register_styles( $this->get_styles() );
	}

	/**
	 * Register scripts
	 *
	 * @param  array $scripts
	 *
	 * @return void
	 */
	private function register_scripts( $scripts ) {
		foreach ( $scripts as $handle => $script ) {
			$deps      = isset( $script['deps'] ) ? $script['deps'] : false;
			$in_footer = isset( $script['in_footer'] ) ? $script['in_footer'] : false;
			$version   = isset( $script['version'] ) ? $script['version'] : LOCATIONS_PLUGIN_VERSION;

			wp_register_script( $handle, $script['src'], $deps, $version, $in_footer );
		}
	}

	/**
	 * Localize admin script
	 */
	public function localize_admin_script() {
		wp_localize_script(
			'locationsplugin-admin',
			'locationsLocal',
			[
				'siteUrl'  => esc_url( home_url() ),
				'ajaxUrl'  => admin_url( 'admin-ajax.php' ),
				'nonce'    => wp_create_nonce( 'wp_rest' ),
			]
		);
	}

	/**
	 * Localize frontend script
	 */
	public function localize_frontend_script() {
		// echo 'do localize';
		wp_localize_script(
			'location-finder',
			'locationsLocal',
			[
				'siteUrl'  => esc_url( home_url() ),
				'closed' => esc_html__('Closed', 'locationFinder'),
				'get_directions' => esc_html__('Get Directions', 'locationFinder'),
				'twenty_four_hours' => esc_html__('Open 24 Hours', 'locationFinder'),
				'self_service' => esc_html__('Self Service Available', 'locationFinder'),
				'handicap_access' => esc_html__('Handicap Access', 'locationFinder'),
				'cash' => esc_html__('Accepts Cash', 'locationFinder'),
				'deposit' => esc_html__('Accept Deposit','locationFinder')


			]
		);
	}

	/**
	 * Register styles
	 *
	 * @param array $styles
	 *
	 * @return void
	 */
	public function register_styles( $styles ) {
		foreach ( $styles as $handle => $style ) {
			$deps = isset( $style['deps'] ) ? $style['deps'] : false;

			wp_register_style( $handle, $style['src'].'?v='.$style['version'], $deps );
		}
	}

	/**
	 * Get all registered scripts
	 *
	 * @return array
	 */
	public function get_scripts() {
		$prefix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '.min' : '';

		$scripts = [
			'locationsplugin-vendor' => [
				'src'       => LOCATIONS_PLUGIN_ASSETS . '/js/vendor.min.js',
				'version'   => filemtime( LOCATIONS_PLUGIN_PATH . '/assets/js/vendor.min.js' ),
				'in_footer' => true,
			],
			'locations-plugin-frontend' => [
				'src'       => LOCATIONS_PLUGIN_ASSETS . '/js/frontend.min.js?v=1.01',
				'deps'      => [ 'jquery', 'locationsplugin-vendor' ],
				'version'   => filemtime( LOCATIONS_PLUGIN_PATH . '/assets/js/frontend.min.js' ),
				'in_footer' => false,
			],
			// 'locationsplugin-admin' => [
			// 	'src'       => LOCATIONS_PLUGIN_ASSETS . '/js/admin.min.js',
			// 	'deps'      => [ 'jquery', 'locationsplugin-vendor' ],
			// 	'version'   => filemtime( LOCATIONS_PLUGIN_PATH . '/assets/js/admin.min.js' ),
			// 	'in_footer' => true,
			// ],
			// 'font-awesome' => [
			// 	'src'       => 'https://use.fontawesome.com/releases/v5.1.0/js/all.js',
			// 	'deps'      => [],
			// 	'version'   => '5.1.0',
			// 	'in_footer' => true,
			// ],
			// 'polyfill-io' => [
			// 	'src'       => 'https://cdn.polyfill.io/v2/polyfill.min.js',
			// 	'deps'      => [],
			// 	'version'   => '3.25.1',
			// 	'in_footer' => false,
			// ],
		];

		return $scripts;
	}

	/**
	 * Get registered styles
	 *
	 * @return array
	 */
	public function get_styles() {

		$styles = [
			'locationsplugin-style' => [
				'src' =>  LOCATIONS_PLUGIN_ASSETS . '/css/style.css',
				'version' => filemtime( LOCATIONS_PLUGIN_PATH . '/assets/css/style.css')
			],
			'locations-plugin-frontend' => [
				'src' =>  LOCATIONS_PLUGIN_ASSETS . '/frontend/css/frontend.css',
				'version' => filemtime( LOCATIONS_PLUGIN_PATH . '/assets/css/frontend.css')
			],
			'locationsplugin-admin' => [
				'src' =>  LOCATIONS_PLUGIN_ASSETS . '/css/admin.css',
				'version' => filemtime( LOCATIONS_PLUGIN_PATH . '/assets/css/admin.css')
			],
		];

		return $styles;
	}

}
