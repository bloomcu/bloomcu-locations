<?php
/**
 * Index
 * Entry point for all templates.
 *
 */

$files = [
    'location-finder.php'
];

foreach ( $files as $file ) {
	require_once $file;
}
