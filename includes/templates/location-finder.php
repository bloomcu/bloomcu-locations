<?php
/**
 * Template Name
 *
 */

function location_finder() {


	/**
	* Enabled Filters
	*
	*/
	$enable_branches = get_field( 'enable_branches', 'option' );
	$enable_atms = get_field( 'enable_atms', 'option' );

	$enable_coop_branches = get_field( 'enable_coop_branches', 'option' );
	$activate_coop_branches = get_field( 'activate_coop_branches', 'option' );

	$enable_coop_atms = get_field( 'enable_coop_atms', 'option' );
	$activate_coop_atms = get_field( 'activate_coop_atms', 'option' );

	$enable_walgreens = get_field( 'enable_walgreens', 'option' );
	$activate_walgreens = get_field( 'activate_walgreens', 'option' );

	// Labels
	$hours_label = get_field( 'hours_label', 'option' );
	$dt_hours_label = get_field( 'drive-thru__hours_label', 'option' );

	// How many filters are enabled?
	$filters = array($enable_branches, $enable_atms, $enable_coop_branches, $enable_coop_atms, $enable_walgreens);
	$filters_enabled_count = count( array_filter($filters) );

	$map_styles = get_map_styles(get_field('map_styles', 'option'));


	/**
	* General
	*
	*/

	// Locator title
	$locator_title = get_field('locator_title', 'option');

	// Near me icon
	$map_near_me_icon = get_field('map_near_me_icon', 'option');

	// Branches icons
	if ($enable_branches) {
		$filter_our_branches_title = get_field('filter_our_branches_title', 'option');
		$filter_our_branches_icon = get_field('filter_our_branches_icon', 'option');
		$map_our_branches_icon = get_field('map_our_branches_icon', 'option');
	}

	// ATMs icons
	if ($enable_atms) {
		$filter_our_atms_title = get_field('filter_our_atms_title', 'option');
		$filter_our_atms_icon = get_field('filter_our_atms_icon', 'option');
		$map_our_atms_icon = get_field('map_our_atms_icon', 'option');
	}

	// COOP Branches icons
	if ($enable_coop_branches) {
		$filter_coop_branches_icon = get_field('filter_coop_branches_icon', 'option');
		$map_coop_branches_color = get_field('map_coop_branches_color', 'option');
	}

	// COOP ATMs icons
	if ($enable_coop_atms) {
		$filter_coop_atms_icon = get_field('filter_coop_atms_icon', 'option');
		$map_coop_atms_color = get_field('map_coop_atms_color', 'option');
	}

	// Walgreens
	if ($enable_walgreens) {
		$filter_walgreens_icon = get_field('filter_walgreens_icon', 'option');
		$map_walgreens_icon = get_field('map_walgreens_icon', 'option');
	}

	/**
	* Map
	*
	*/
	$starting_coords = get_field('map_starting_location', 'option');
	$starting_lat = $starting_coords['lat'];
	$starting_lng = $starting_coords['lng'];
	$starting_zoom = get_field('map_starting_zoom', 'option');


	/**
	* Search Radius
	*
	*/
	$radius_plugin = get_field('radius_plugin', 'option');
	$radius_coop = get_field('radius_coop', 'option');
	$sort_by = esc_attr(get_field( 'sort_by', 'option' ));
	$sort_order = esc_attr(get_field( 'sort_order', 'option' ));

	?>

	<script>

		/**
		* Setup variables for location finder script
		*
		*/

		var locations_data_layer = {

			/**
			* General
			*
			*/

			// Site url
		    'site_url' : '<?php echo esc_url( home_url() ); ?>',

			// Near me icon
			'map_near_me_icon' : '<?php echo $map_near_me_icon["url"]; ?>',

			// Branches icons
		    <?php if ($enable_branches) { ?>
				'filter_our_branches_icon' : '<?php echo $filter_our_branches_icon["url"]; ?>',
				'map_our_branches_icon' : '<?php echo $map_our_branches_icon["url"]; ?>',
			<?php } ?>

			// ATMs icons
			<?php if ($enable_atms) { ?>
			    'filter_our_atms_icon' : '<?php echo $filter_our_atms_icon["url"]; ?>',
				'map_our_atms_icon' : '<?php echo $map_our_atms_icon["url"]; ?>',
			<?php } ?>

			// COOP Branches icons
			<?php if ($enable_coop_branches) { ?>
				'filter_coop_branches_icon' : '<?php echo $filter_coop_branches_icon["url"]; ?>',
				'map_coop_branches_color' : '<?php echo $map_coop_branches_color; ?>',
			<?php } ?>

			// COOP ATMs icons
			<?php if ($enable_coop_atms) { ?>
			    'filter_coop_atms_icon' : '<?php echo $filter_coop_atms_icon["url"]; ?>',
				'map_coop_atms_color' : '<?php echo $map_coop_atms_color; ?>',
			<?php } ?>

			// Walgreens icons
			<?php if ($enable_walgreens) { ?>
			    'filter_walgreens_icon' : '<?php echo $filter_walgreens_icon["url"]; ?>',
				'map_walgreens_icon' : '<?php echo $map_walgreens_icon["url"]; ?>',
			<?php } ?>

			/**
			* Map
			*
			*/
		    'starting_lat' : '<?php echo $starting_lat; ?>',
		    'starting_lng' : '<?php echo $starting_lng; ?>',
			'starting_zoom' : '<?php echo $starting_zoom; ?>',
			'map_styles'    : <?php echo $map_styles ? $map_styles : '[]';?>,

			/**
			* Search Radius
			*
			*/
		    'radius_plugin' : '<?php echo $radius_plugin; ?>',
		    'radius_coop' : '<?php echo $radius_coop; ?>',
			'sort_by' :'<?php echo $sort_by;?>',
			'sort_order' :'<?php echo $sort_order;?>',
			/**
			* Enable Filters
			*
			*/
			'enable_branches' : '<?php echo $enable_branches; ?>',
			'enable_atms' : '<?php echo $enable_atms; ?>',
			'enable_coop_branches' : '<?php echo $enable_coop_branches; ?>',
			'enable_coop_atms' : '<?php echo $enable_coop_atms; ?>',
			'enable_walgreens' : '<?php echo $enable_walgreens; ?>',
		}
	</script>
	<style>
		html.js #bloom-widget .showcase {
			/* opacity: 0;
			transition: 1000ms ease opacity; */
		}
		html.js #bloom-widget.loading .showcase{
			/* opacity: 0; */
		}
		html.js #bloom-widget.loaded .showcase{
			/* opacity: 1; */
		}
		html.js #bloom-widget.loaded .bloom-widget-loader {
			display: none;
		}

		.bloom-widget-loader {
			position: absolute;
			top: 0;
			left: 0;
			z-index: 999;
			width: 100%;

		}
		.bloom-widget-loader__container {
			width: 100%;
			height: 5px;
			background: #efefef;
		}
		.bloom-widget-loader__track {
			width: 0%;
			height: 100%;
			background: var(--color-primary);
			transition: 500ms ease width;
		}
		
	</style>
	<div id="bloom-widget" style="position: relative;" class="bloomcu-locations">
		<div class="bloom-widget-loader">
			<div class="bloom-widget-loader__container">
				<div class="bloom-widget-loader__track"></div>
			</div>
		</div>
	    <!-- Showcases -->
	    <section class="showcase">

	        <!-- Left: Nav -->
	        <div class="nav-container">

	            <div class="nav-filters-container">
	                <div class="h3 title-find"><?php echo $locator_title; ?></div>

	                <div class="search-near-me-container">

						<div class="search-container">
							<div>
								<form onsubmit="searchBranches(event);" class="search-inner-container">
								<div class="search-form">
									<input type="text" class="search-input" placeholder="<?php _e('City & State or Zip', 'locationFinder'); ?>" aria-label="<?php _e('Search City or Zip', 'locationFinder'); ?>"/>
								</div>
								<button type="submit" class="svg-button search-button" aria-label="Search branches" onclick="searchBranches(event);" >
									<svg class="svg-icon" viewBox="0 0 20 20">
										<path fill="none" d="M19.129,18.164l-4.518-4.52c1.152-1.373,1.852-3.143,1.852-5.077c0-4.361-3.535-7.896-7.896-7.896c-4.361,0-7.896,3.535-7.896,7.896s3.535,7.896,7.896,7.896c1.934,0,3.705-0.698,5.078-1.853l4.52,4.519c0.266,0.268,0.699,0.268,0.965,0C19.396,18.863,19.396,18.431,19.129,18.164z M8.567,15.028c-3.568,0-6.461-2.893-6.461-6.461s2.893-6.461,6.461-6.461c3.568,0,6.46,2.893,6.46,6.461S12.135,15.028,8.567,15.028z"></path>
									</svg>
								</button>
								<button aria-label="Clear searched branches" class="svg-button clear-search-button" type="button" onclick="clearSearchBranches(event);">
									<svg class="svg-icon" viewBox="0 0 20 20">
										<path fill="none" d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path>
									</svg>
								</button>
								</form>								
							</div>
							<div class="no-search-results" style="display:none;">
								<?php _e('No results, please try again!', 'locationFinder'); ?>
							</div>
						</div>

						<button class="nav-filter near-me" onclick="nearMe(this)">
							<svg class="svg-icon checkmark-icon" viewBox="0 0 20 20">
								<path fill="none" d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
							</svg>
							<svg class="svg-icon spinner" viewBox="0 0 20 20">
								<path fill="none" d="M19.305,9.61c-0.235-0.235-0.615-0.235-0.85,0l-1.339,1.339c0.045-0.311,0.073-0.626,0.073-0.949
								c0-3.812-3.09-6.901-6.901-6.901c-2.213,0-4.177,1.045-5.44,2.664l0.897,0.719c1.053-1.356,2.693-2.232,4.543-2.232
								c3.176,0,5.751,2.574,5.751,5.751c0,0.342-0.037,0.675-0.095,1l-1.746-1.39c-0.234-0.235-0.614-0.235-0.849,0
								c-0.235,0.235-0.235,0.615,0,0.85l2.823,2.25c0.122,0.121,0.282,0.177,0.441,0.172c0.159,0.005,0.32-0.051,0.44-0.172l2.25-2.25
								C19.539,10.225,19.539,9.845,19.305,9.61z M10.288,15.752c-3.177,0-5.751-2.575-5.751-5.752c0-0.276,0.025-0.547,0.062-0.813
								l1.203,1.203c0.235,0.234,0.615,0.234,0.85,0c0.234-0.235,0.234-0.615,0-0.85l-2.25-2.25C4.281,7.169,4.121,7.114,3.961,7.118
								C3.802,7.114,3.642,7.169,3.52,7.291l-2.824,2.25c-0.234,0.235-0.234,0.615,0,0.85c0.235,0.234,0.615,0.234,0.85,0l1.957-1.559
								C3.435,9.212,3.386,9.6,3.386,10c0,3.812,3.09,6.901,6.902,6.901c2.083,0,3.946-0.927,5.212-2.387l-0.898-0.719
								C13.547,14.992,12.008,15.752,10.288,15.752z"></path>
							</svg>
							<svg class="svg-icon near" viewBox="0 0 20 20">
								<path d="M17.659,9.597h-1.224c-0.199-3.235-2.797-5.833-6.032-6.033V2.341c0-0.222-0.182-0.403-0.403-0.403S9.597,2.119,9.597,2.341v1.223c-3.235,0.2-5.833,2.798-6.033,6.033H2.341c-0.222,0-0.403,0.182-0.403,0.403s0.182,0.403,0.403,0.403h1.223c0.2,3.235,2.798,5.833,6.033,6.032v1.224c0,0.222,0.182,0.403,0.403,0.403s0.403-0.182,0.403-0.403v-1.224c3.235-0.199,5.833-2.797,6.032-6.032h1.224c0.222,0,0.403-0.182,0.403-0.403S17.881,9.597,17.659,9.597 M14.435,10.403h1.193c-0.198,2.791-2.434,5.026-5.225,5.225v-1.193c0-0.222-0.182-0.403-0.403-0.403s-0.403,0.182-0.403,0.403v1.193c-2.792-0.198-5.027-2.434-5.224-5.225h1.193c0.222,0,0.403-0.182,0.403-0.403S5.787,9.597,5.565,9.597H4.373C4.57,6.805,6.805,4.57,9.597,4.373v1.193c0,0.222,0.182,0.403,0.403,0.403s0.403-0.182,0.403-0.403V4.373c2.791,0.197,5.026,2.433,5.225,5.224h-1.193c-0.222,0-0.403,0.182-0.403,0.403S14.213,10.403,14.435,10.403"></path>
							</svg>
							<span><?php _e('Near Me', 'locationFinder'); ?></span>
						</button>
						<?php if( get_field('info_modal_content', 'option') ) :?>	
							<a href="#locations-info-modal" class="search-info-trigger search-info">
								<span>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-icon" style="width:16px; height:16px;">
										<path d="M80 160c0-35.3 28.7-64 64-64l32 0c35.3 0 64 28.7 64 64l0 3.6c0 21.8-11.1 42.1-29.4 53.8l-42.2 27.1c-25.2 16.2-40.4 44.1-40.4 74l0 1.4c0 17.7 14.3 32 32 32s32-14.3 32-32l0-1.4c0-8.2 4.2-15.8 11-20.2l42.2-27.1c36.6-23.6 58.8-64.1 58.8-107.7l0-3.6c0-70.7-57.3-128-128-128l-32 0C73.3 32 16 89.3 16 160c0 17.7 14.3 32 32 32s32-14.3 32-32zm80 320a40 40 0 1 0 0-80 40 40 0 1 0 0 80z"/>
									</svg>
								</span>
								<span class="h-visual-hide sr-only">More information on map features and locations</span>
							</a>
						<?php endif; ?>
	                </div>

	                <div class="nav-filters-group">
						<?php if ( $filters_enabled_count > 0) { ?>

							<?php if ($enable_branches) { ?>
								<button class="nav-filter branch active" onclick="getBranchesByType(this)">
									<div class="nav-filter-inner-wrapper">
										<svg class="svg-icon checkmark-icon" viewBox="0 0 20 20">
											<path fill="none" d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
										</svg>
										<!-- <img alt="" src="<?php echo $filter_our_branches_icon['url'] ?>" class="pin"> -->
										<?php echo empty($filter_our_branches_icon['url']) ? '<br/>' : 
										'<img role="presentation" alt="" src="'.$filter_our_branches_icon["url"].'" class="pin">'; ?>
										<p><?php echo $filter_our_branches_title ?></p>
									</div>
								</button>
							<?php } ?>

							<?php if ($enable_atms) { ?>
								<button class="nav-filter atm active"  onclick="getBranchesByType(this)">
									<div class="nav-filter-inner-wrapper">
										<svg class="svg-icon checkmark-icon" viewBox="0 0 20 20">
											<path fill="none" d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
										</svg>
										<!-- <img alt="" src="<?php // echo $filter_our_atms_icon['url'] ?>" class="pin"> -->
										<?php echo empty($filter_our_atms_icon['url']) ? '<br/>' : 
										'<img role="presentation" alt="" src="'.$filter_our_atms_icon["url"].'" class="pin">'; ?>
										
										<p><?php echo $filter_our_atms_title ?></p>
									</div>
								</button>
							<?php } ?>

							<?php if ($enable_coop_branches) { ?>
								<button aria-pressed="false" class="nav-filter nav-filter-last shared-branch<?php echo ($activate_coop_branches ? ' active': ''); ?>" onclick="getBranchesByType(this)">
									<div class="nav-filter-inner-wrapper">
										<svg class="svg-icon checkmark-icon" viewBox="0 0 20 20">
											<path fill="none" d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
										</svg>
										<!-- <img role="presentation" src="<?php echo $filter_coop_branches_icon['url'] ?>" class="dot"> -->
										<?php echo empty($filter_coop_branches_icon['url']) ? '<br/>' : 
										'<img role="presentation" alt="" src="'.$filter_coop_branches_icon["url"].'" class="dot">'; ?>
										<p><?php _e('Shared', 'locationFinder'); ?><br><?php _e('Branches', 'locationFinder'); ?></p>
									</div>
								</button>
							<?php } ?>

							<?php if ($enable_coop_atms) { ?>
								<button class="nav-filter shared-atm<?php echo ($activate_coop_atms ? ' active': ''); ?>" onclick="getBranchesByType(this)">
									<div class="nav-filter-inner-wrapper">
										<svg class="svg-icon checkmark-icon" viewBox="0 0 20 20">
											<path fill="none" d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
										</svg>
										<!-- <img role="presentation" src="<?php echo $filter_coop_atms_icon['url'] ?>" class="dot"> -->
										<?php echo empty($filter_coop_atms_icon['url']) ? '<br/>' : 
										'<img role="presentation" alt="" src="'.$filter_coop_atms_icon["url"].'" class="dot">'; ?>
										<p><?php _e('Shared', 'locationFinder'); ?><br><?php _e('ATMs', 'locationFinder'); ?></p>
									</div>
								</button>
							<?php } ?>

							<?php if ($enable_walgreens) { ?>
								<button class="nav-filter walgreens<?php echo ($activate_walgreens ? ' active': ''); ?>" onclick="getBranchesByType(this)">
									<div class="nav-filter-inner-wrapper">
										<svg class="svg-icon checkmark-icon" viewBox="0 0 20 20">
											<path fill="none" d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
										</svg>
										<?php echo empty($filter_walgreens_icon['url']) ? '<br/>' : '<img role="presentation" alt="" src="'.$filter_walgreens_icon["url"].'" class="dot">'; ?>
										<p><?php _e('Walgreens', 'locationFinder'); ?><br><?php _e('ATMs', 'locationFinder'); ?></p>
									</div>
								</button>
							<?php } ?>


						<?php } ?>

					</div>

	            </div>

	            <div class="branch-list-container"></div>

	        </div><!-- End .nav-container -->

	        <div class="branch-info-container" hidden>

	            <div class="branch-close-container">
	                <button aria-label="Close Branch Information" class="svg-button close-button" type="button" onclick="closeBranchInfo();">
	                    <svg class="svg-icon" viewBox="0 0 20 20">
	                        <path fill="none" d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path>
	                    </svg>
	                </button>
	            </div>

	            <div class="branch-header">
					
	                <div class="h3 branch-title"></div>
	                <div class="h5 branch-subtitle"></div>
	                <!-- <img role="presentation" src="" class="branch-img"> -->
	                <span class="block branch-address"></span>
	                <span class="block branch-address2"></span>
					<div class="branch-photo-wrap"></div>
	                <div class="block branch-phone"></div>
	                <div class="block branch-direction"></div>
	            </div>

	            <div class="branch-body">

					<div class="branch-hours-container">
						<div class="h5">
						<?php _e($hours_label, 'locationFinder'); ?>
						</div>
						<table class="info-table branch_hours" role="presentation">
							<tr>
								<td><?php _e('Monday', 'locationFinder'); ?></td>
								<td class="branch-monday"></td>
							</tr>
							<tr>
								<td><?php _e('Tuesday', 'locationFinder'); ?></td>
								<td class="branch-tuesday"></td>
							</tr>
							<tr>
								<td><?php _e('Wednesday', 'locationFinder'); ?></td>
								<td class="branch-wednesday"></td>
							</tr>
							<tr>
								<td><?php _e('Thursday', 'locationFinder'); ?></td>
								<td class="branch-thursday"></td>
							</tr>
							<tr>
								<td><?php _e('Friday', 'locationFinder'); ?></td>
								<td class="branch-friday"></td>
							</tr>
							<tr>
								<td><?php _e('Saturday', 'locationFinder'); ?></td>
								<td class="branch-saturday"></td>
							</tr>
							<tr>
								<td><?php _e('Sunday', 'locationFinder'); ?></td>
								<td class="branch-sunday"></td>
							</tr>
						</table>
					</div>

					<div class="branch-drive-thru-hours-container">
						<div class="h5"><?php _e( $dt_hours_label, 'locationFinder' ); ?></div>
						<table class="info-table drive_thru_hours" role="presentation">
							<tr>
								<td><?php _e('Monday', 'locationFinder'); ?></td>
								<td class="branch-drive-monday"></td>
							</tr>
							<tr>
								<td><?php _e('Tuesday', 'locationFinder'); ?></td>
								<td class="branch-drive-tuesday"></td>
							</tr>
							<tr>
								<td><?php _e('Wednesday', 'locationFinder'); ?></td>
								<td class="branch-drive-wednesday"></td>
							</tr>
							<tr>
								<td><?php _e('Thursday', 'locationFinder'); ?></td>
								<td class="branch-drive-thursday"></td>
							</tr>
							<tr>
								<td><?php _e('Friday', 'locationFinder'); ?></td>
								<td class="branch-drive-friday"></td>
							</tr>
							<tr>
								<td><?php _e('Saturday', 'locationFinder'); ?></td>
								<td class="branch-drive-saturday"></td>
							</tr>
							<tr>
								<td><?php _e('Sunday', 'locationFinder'); ?></td>
								<td class="branch-drive-sunday"></td>
							</tr>
						</table>
					</div>

					<div class="branch-phone-numbers-container">
						<div class="h5"><?php _e('Phone Number', 'locationFinder'); ?></div>
						<table class="info-table branch-phones" role="presentation"></table>
					</div>

					<div class="branch-websites-container">
						<div class="h5"><?php _e('Website', 'locationFinder'); ?></div>
						<table class="info-table branch-website" role="presentation"></table>
					</div>

					<div class="branch-services-container">
						<div class="h5"><?php _e('Services', 'locationFinder'); ?></div>
						<table class="info-table services branch-services" role="presentation"></table>
					</div>

				</div><!-- /.branch-body -->
	        </div><!-- /.branch-info-container -->

	        <div class="other-info-container" hidden>
	            <div class="other-info-close-container">
	                <button aria-label="Close information" class="svg-button close-button" type="button" onclick="closeOtherInfo();">
	                    <svg class="svg-icon" viewBox="0 0 20 20">
	                        <path fill="none" d="M15.898,4.045c-0.271-0.272-0.713-0.272-0.986,0l-4.71,4.711L5.493,4.045c-0.272-0.272-0.714-0.272-0.986,0s-0.272,0.714,0,0.986l4.709,4.711l-4.71,4.711c-0.272,0.271-0.272,0.713,0,0.986c0.136,0.136,0.314,0.203,0.492,0.203c0.179,0,0.357-0.067,0.493-0.203l4.711-4.711l4.71,4.711c0.137,0.136,0.314,0.203,0.494,0.203c0.178,0,0.355-0.067,0.492-0.203c0.273-0.273,0.273-0.715,0-0.986l-4.711-4.711l4.711-4.711C16.172,4.759,16.172,4.317,15.898,4.045z"></path>
	                    </svg>
	                </button>
	            </div>
	            <div class="other-info-header">
	                <div class="h3 other-info-title"><?php _e('Other Info', 'locationFinder'); ?></div>
	                <div class="h5 other-info-subtitle"><?php _e('Subtitle', 'locationFinder'); ?></div>
	            </div>
	            <div class="other-info-body"></div>
	        </div>

	        <!-- Map -->
	        <div class="map-container">
	            <div id="map"></div>
	        </div>
			<?php if( get_field('info_modal_content', 'option') ) :
				
			?>
			<?php add_action('wp_footer', function(){ ?>
				<div
					id="locations-info-modal"
					class="c-off-canvas c-off-canvas--small"
					data-modal="locations-info-modal"
					aria-hidden="true"
				>
					<div class="c-off-canvas__inner">
						<button class="c-off-canvas__close" aria-label="Close this popup" data-close-modal>
							<span class="c-off-canvas__close-icon">
								<i class="fal fa-times"></i>
								<span class="c-off-canvas__close-text">Close</span>
							</span>
						</button>

						<div class="c-off-canvas__content">
							<?php if( get_field('info_modal_title', 'option') ) :?>
								<h3 class="c-off-canvas__title">
									<?php echo esc_html(  get_field( 'info_modal_title', 'option' ) );?>
								</h3>
							<?php endif;?>
							<div class="c-off-canvas__content-inner">
								<?php echo wp_kses_post( get_field( 'info_modal_content', 'option') );?> 
							</div>
						</div>
					</div>
				</div>
			<?php }); ?>
				
			<?php endif;?>
	    </section>
	</div>
	
	<?php
}
if( ! function_exists( 'get_map_styles') ) {
	function get_map_styles($key) {
		switch($key){
			case 'silver':
				return '[
					{
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#f5f5f5"
						}
					  ]
					},
					{
					  "elementType": "labels.icon",
					  "stylers": [
						{
						  "visibility": "off"
						}
					  ]
					},
					{
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#616161"
						}
					  ]
					},
					{
					  "elementType": "labels.text.stroke",
					  "stylers": [
						{
						  "color": "#f5f5f5"
						}
					  ]
					},
					{
					  "featureType": "administrative.land_parcel",
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#bdbdbd"
						}
					  ]
					},
					{
					  "featureType": "poi",
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#eeeeee"
						}
					  ]
					},
					{
					  "featureType": "poi",
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#757575"
						}
					  ]
					},
					{
					  "featureType": "poi.park",
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#e5e5e5"
						}
					  ]
					},
					{
					  "featureType": "poi.park",
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#9e9e9e"
						}
					  ]
					},
					{
					  "featureType": "road",
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#ffffff"
						}
					  ]
					},
					{
					  "featureType": "road.arterial",
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#757575"
						}
					  ]
					},
					{
					  "featureType": "road.highway",
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#dadada"
						}
					  ]
					},
					{
					  "featureType": "road.highway",
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#616161"
						}
					  ]
					},
					{
					  "featureType": "road.local",
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#9e9e9e"
						}
					  ]
					},
					{
					  "featureType": "transit.line",
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#e5e5e5"
						}
					  ]
					},
					{
					  "featureType": "transit.station",
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#eeeeee"
						}
					  ]
					},
					{
					  "featureType": "water",
					  "elementType": "geometry",
					  "stylers": [
						{
						  "color": "#c9c9c9"
						}
					  ]
					},
					{
					  "featureType": "water",
					  "elementType": "labels.text.fill",
					  "stylers": [
						{
						  "color": "#9e9e9e"
						}
					  ]
					}
				  ]';
				break;
			default:
				return '[]';
				break;
		}
	}
}

