<?php
namespace Locations;

/**
 * REST_API Handler
 */
class Ajax {

	public function __construct() {
		add_action( 'wp_ajax_locations_save_locations', [ $this, 'save_locations' ] );
	}

	public function save_locations() {

		$post_id   = sanitize_text_field( $_POST['id'] );
		$locations_type = sanitize_text_field( $_POST['type'] );
		$locations_json = $_POST['schema'];

		if ( empty( $post_id ) || empty( $locations_json ) ) {
			return;
		}

		$this->save_postdata( $post_id, $locations_json, $locations_type );

		wp_die();

	}

	/**
	 * Save Quiz Data
	 */
	public function save_postdata( $post_id, $schema, $type ) {

		update_post_meta(
			$post_id,
			'_bloom_locations_schema',
			wp_slash( $schema )
		);

		update_post_meta(
			$post_id,
			'_bloom_locations_type',
			$type
		);

	}

}
