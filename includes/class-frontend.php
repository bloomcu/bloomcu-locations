<?php
namespace Locations;
require 'templates/index.php';
// require 'partials/index.php';

/**
 * Frontend Handler
 */
class Frontend {

	public function __construct() {
		wp_register_script( 'location-finder',  LOCATIONS_PLUGIN_ASSETS . '/frontend/js/location-finder.js?v='.LOCATIONS_PLUGIN_VERSION, [], false, true );
		wp_register_script( 'maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAwOXyC6lxFn0OkRzISyeiDDr0E_RyBJPQ&callback=initMap', ['location-finder'], false, true);
		add_shortcode( 'location-finder', [ $this, 'render_location_finder' ] );

	}

	/**
	 * Get all locations
	 *
	 */
	public function get_all_locations() {
		$sort_by = esc_attr(get_field( 'sort_by', 'option' ));
		$sort_order = esc_attr(get_field( 'sort_order', 'option' ));
		$allowed_orders = ['ASC', 'DESC'];

		$args = [
			'post_type' => 'location',
			'posts_per_page' => -1,
		];

		if( $sort_by && $sort_by === 'type' ) {
			$args['orderby'] = 'meta_value';
			$args['meta_key'] = 'type';
		}

		if( in_array( $sort_order, $allowed_orders) ) {
			$args['order'] = $sort_order;
		}
		



		/**
		 * Run query
		 *
		 * "\" reaches into global namespace for WP_Query
		 */
		$query = new \WP_Query($args);

		return $query;

	}
	/**
	 * Get only locations that are flagged for display
	 *
	 */
	public function get_list_view_locations() {

		$args = [
			'post_type' => 'location',
			'posts_per_page' => -1,
			'meta_query'	=> array(
				array(
					'key'	  	=> 'show_in_list_view',
					'value'	  	=> '1',
					'compare' 	=> '=',
				),
			),
			'orderby' => 'title',
			'order'	=> 'ASC'
		];

		/**
		 * Run query
		 *
		 * "\" reaches into global namespace for WP_Query
		 */
		$query = new \WP_Query($args);

		return $query;

	}

	/**
	 * Get nearby locations
	 *
	 */
	public function get_nearby_locations( $params ) {

		// Setup arguments from url parameters
		$latitudeFrom = $params['latitudeFrom'];
		$longitudeFrom = $params['longitudeFrom'];
		$search_radius = $params['search_radius'];

		// Query all locations
		$query = $this->get_all_locations();

		// Array to hold our results
	    $results = array();

		// Loop through each location
		while( $query->have_posts() ) {

			// Setup location post data
	        $query->the_post();

			// Get location coords from Google Map ACF
			$location_coords = get_field( 'google_map' );

			// Setup location coords
			$latitudeTo = $location_coords['lat'];
			$longitudeTo = $location_coords['lng'];

			// Get distance from starting point to location
			$distance = $this->get_distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo);

			// Add distance to location post
			// Round distance to one decimal place
			$query->post->distance = round($distance, 1);

			// Check location is in search radius
			if ( $distance <= $search_radius ) {

				// Add this location to results
				array_push($results, $query->post);

			}

		}

		return $results;

	}

	/**
	 * Render Location Finder
	 *
	 * @param  array $atts
	 * @return string
	 */
	public function render_location_finder( $args ) {
		wp_enqueue_style( 'locations-plugin-frontend' );
		wp_enqueue_script( 'location-finder');
		wp_enqueue_script( 'maps');

		/**
		* Render
		* -----------------
		*/
		ob_start(); // Start buffering

		echo location_finder();

		return ob_get_clean(); // Return contents of buffer
	}

	/**
	 * Get Distance
	 *
	 * Calculates the great-circle distance between two points, with the Haversine formula.
	 * @param float $latitudeFrom Latitude of start point in [deg decimal]
	 * @param float $longitudeFrom Longitude of start point in [deg decimal]
	 * @param float $latitudeTo Latitude of target point in [deg decimal]
	 * @param float $longitudeTo Longitude of target point in [deg decimal]
	 * @return float Distance between points in [miles]
	 */
	public function get_distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo) {

		// Mean earth radius in miles
		$earthRadius = 3958.8;

		// Convert from degrees to radians
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);

		// Get deltas
		$latDelta = $latTo - $latFrom;
		$lonDelta = $lonTo - $lonFrom;

		// Get angle
		$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

		return $angle * $earthRadius;
	}

}
